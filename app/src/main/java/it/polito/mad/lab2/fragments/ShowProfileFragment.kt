package it.polito.mad.lab2.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import it.polito.mad.lab2.R
import it.polito.mad.lab2.classes.Adds
import it.polito.mad.lab2.classes.AddsArray
import it.polito.mad.lab2.classes.UserInfo
import kotlinx.android.synthetic.main.fragment_1_show_profile.*
import java.lang.reflect.Type


/**
 * A simple [Fragment] subclass.
 */
class ShowProfileFragment : Fragment() {

    //Live data app attributes
    var profileImg_value = MutableLiveData<String>()
    var fullName_value = MutableLiveData<String>()
    var nickName_value = MutableLiveData<String>()
    var email_value = MutableLiveData<String>()
    var location_value = MutableLiveData<String>()
    var userRating_value = MutableLiveData<Double>()
    var itemBoughtNumber_value = MutableLiveData<Int>()

    //Variables for holding instance data
    private var SHARED_PREFS: String = "sharedPrefs"



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                        savedInstanceState: Bundle?): View? {

        //Getting reference to access to local file
        val sharedPref = requireActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

        //Checking for persisted data
        if (sharedPref.getBoolean("notEmpty",false)) {

            // Extracting Json data Object
            val storage = sharedPref.getString("userPersist", "")
            val gSon = Gson()
            val jsonUserInfo = gSon.fromJson(storage,UserInfo::class.java)

            profileImg_value.value = jsonUserInfo.profileImg
            fullName_value.value = jsonUserInfo.fullName
            nickName_value.value = jsonUserInfo.nickName
            email_value.value = jsonUserInfo.email
            location_value.value = jsonUserInfo.location

        }else{

            profileImg_value.value =  Uri.parse(
                "android.resource://" + R::class.java.getPackage()!!.name + "/" + R.drawable.avatar1).toString()
            fullName_value.value = getResources().getString(R.string.fullName_default)
            nickName_value.value = getResources().getString(R.string.nickName_default)
            email_value.value = getResources().getString(R.string.email_default)
            location_value.value = getResources().getString(R.string.location_default)

        }

        userRating_value.value = getResources().getString(R.string.userRating_default).toDouble()
        itemBoughtNumber_value.value = getResources().getString(R.string.itemBoughtNumber_default).toInt()

        // Inflate the layout for this fragment
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_1_show_profile, container, false)
    }

    /*************************************
     ************* On Start **************
     *************************************/

    //Capturing data from user on real-time
    override fun onStart() {
        super.onStart()
        profileImg_value.observe(this, Observer { imageView8.setImageURI(Uri.parse(it)) })
        fullName_value.observe(this, Observer { fullName.text = it })
        nickName_value.observe(this, Observer { nickName.text = it })
        email_value.observe(this, Observer { email.text = it })
        location_value.observe(this, Observer { location.text = it })
        userRating_value.observe(this, Observer { tvRating.text = it.toString() })
        itemBoughtNumber_value.observe(this, Observer { tvNumberBought.text = it.toString()  })
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_edit, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    override fun onOptionsItemSelected(menuItem: MenuItem ):Boolean{
        return when (menuItem.itemId) {
            R.id.menu_edit -> {
                var bundle: Bundle = bundleOf(
                    "editFullName" to fullName_value.value,
                    "editNickName" to nickName_value.value,
                    "editEmail" to email_value.value,
                    "editLocation" to location_value.value,
                    "editProfileImg" to profileImg_value.value
                )
                findNavController().navigate(R.id.nav_editProfile,bundle)
                true
            }
            else -> false
        }
    }


}
