package it.polito.mad.lab2.classes

data class Adds(var imgPath: String, var title: String,
                var description: String, var price: String,
                var category: String, var location: String,
                var expireDate: String)

