package it.polito.mad.lab2.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.*
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.polito.mad.lab2.classes.CustomAdapter
import it.polito.mad.lab2.R
import it.polito.mad.lab2.classes.AddsArray.adds
import it.polito.mad.lab2.classes.FragmentName

/**
 * A simple [Fragment] subclass.
 */
class ItemListFragment : Fragment() {

    /***********************************
     ********** Trying out the firebase ********
     ***********************************/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    /***********************************
     ********** On Created View ********
     ***********************************/

    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        val fab: FloatingActionButton = view.findViewById(R.id.floating_action_button)

        //getting recyclerview from xml
        val recyclerView = getView()?.findViewById(R.id.recycler_view) as RecyclerView
        val emptyView: TextView? = getView()?.findViewById(R.id.empty_view)

        if (adds.isEmpty()){
            recyclerView.visibility = View.GONE;
            emptyView?.visibility = View.VISIBLE;
        }
        else {
            recyclerView.visibility = View.VISIBLE;
            emptyView?.visibility = View.GONE;
            //adding a layout manager
            recyclerView.layoutManager = LinearLayoutManager(context, VERTICAL, false)

            //creating adapter
            val adapter = CustomAdapter(FragmentName.MY_ITEM_LIST)

            //now adding the adapter to recyclerview
            recyclerView.adapter = adapter
        }

        fab.setOnClickListener (View.OnClickListener (){
            findNavController().navigate(R.id.action_nav_adds_to_itemEditFragment)
        })
    }

}

