package it.polito.mad.lab2.fragments

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.polito.mad.lab2.R
import it.polito.mad.lab2.classes.AddsArray
import it.polito.mad.lab2.classes.ArrivesToEditItem
import it.polito.mad.lab2.classes.Utils
import kotlinx.android.synthetic.main.fragment_item_details.*

class ItemDetailsFragment : Fragment() {
    /*private var itemTitle = MutableLiveData<String>()
    private var itemDescription = MutableLiveData<String>()
    private var itemPrice = MutableLiveData<Double>()*/

    private var itemIndex:Int = -1

    //function that check if the item is of another user or is of this user
    private fun itemIsOfAnotherUser() : Boolean{
        // TODO move it inside the repository and implement it! Probably it is not the best solution but until the merge of
        //  the two branches this is a temporary solution.
        return true
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(itemIsOfAnotherUser()){
            toolbarItemDetails.menu.findItem(R.id.edit_item).setVisible(false)
            fab_interested.setOnClickListener {
                notifyInterest()
            }
        }
        else {
            toolbarItemDetails.setOnMenuItemClickListener { menuItem ->
                Log.d("Lab2Debug", "optionSelected")
                when (menuItem.itemId) {
                    R.id.edit_item -> {
                        editItem()
                        true
                    }
                    else -> false
                }
            }
            fab_interested.visibility = View.GONE
        }

        //code needed to change toolbar and to make the back button correctly working
        val navHostFragment = NavHostFragment.findNavController(this);
        //NavigationUI.setupWithNavController(toolbarItemDetails, navHostFragment)
        toolbarItemDetails.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        arguments?.let {
            itemIndex = it.getInt(AddsArray.ARG_ITEM_INDEX)
        }

        if(itemIndex != -1){
            val currentItem = AddsArray.adds[itemIndex]
            itemDetailsImageView.setImageURI(Uri.parse(currentItem.imgPath))
            tvTitle.text = currentItem.title
            tvDescription.text = currentItem.description
            tvPrice.text = currentItem.price
            tvCategory.text = currentItem.category
            tvLocation.text = currentItem.location
            tvExpiryDate.text = currentItem.expireDate
        }
    }

    private fun notifyInterest() {
        //TODO("Not yet implemented")
        //I have to notify to the owner of the object that I am interested in buying this item!
    }

    private fun editItem() {

        val b = bundleOf(AddsArray.ARG_ITEM_INDEX to itemIndex)

        Utils.arrivesToEditItem = ArrivesToEditItem.FROM_ITEM_DETAILS

        findNavController().navigate(R.id.itemEditFragment,b)
        //Toast.makeText(context, "Edit Item Clicked", Toast.LENGTH_SHORT).show()
    }

}
