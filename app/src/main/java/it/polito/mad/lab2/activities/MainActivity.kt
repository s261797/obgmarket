package it.polito.mad.lab2.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import it.polito.mad.lab2.R
import it.polito.mad.lab2.classes.Adds
import it.polito.mad.lab2.classes.AddsArray
import it.polito.mad.lab2.classes.ImageCaption
import it.polito.mad.lab2.classes.UserInfo
import java.lang.reflect.Type


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    companion object {
        public val SHARED_PREFS: String = "sharedPrefs"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }*/

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_adds,
                R.id.nav_thrash,
                R.id.nav_profile,
                R.id.nav_editProfile
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //Getting reference to access to local file
        val sharedPref = this.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

        //Checking for persisted data
        if (sharedPref.getBoolean("notEmptyItems", false)) {

            val gson = Gson()
            val jsonOutput = sharedPref.getString("addsArray", "")
            if (jsonOutput != "") {
                Log.e("Lab2Debug", jsonOutput)
                val listType: Type = object : TypeToken<MutableList<Adds>>() {}.type
                AddsArray.adds = gson.fromJson<MutableList<Adds>>(jsonOutput, listType)
            }
        }

        if (sharedPref.getBoolean("notEmpty", false)) {

            // Extracting Json data Object
            val storage = sharedPref.getString("userPersist", "")
            val gSon = Gson()
            val jsonUserInfo = gSon.fromJson(storage, UserInfo::class.java)

            //update drawer header
            val navView: NavigationView = this.findViewById(R.id.nav_view)
            val headerView = navView.getHeaderView(0)
            headerView.findViewById<ImageView>(R.id.imgView_nav_header).setImageURI(Uri.parse(jsonUserInfo.profileImg))
            headerView.findViewById<TextView>(R.id.tv_nav_header_title).text = jsonUserInfo.fullName
            headerView.findViewById<TextView>(R.id.tv_nav_header_subtitle).text = jsonUserInfo.email
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImageCaption.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                             grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        ImageCaption.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}
