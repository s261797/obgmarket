package it.polito.mad.lab2.classes

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

fun hideKeyboard(activity: Activity) {
    val inputMethodManager =
        activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    // Check if no view has focus
    val currentFocusedView = activity.currentFocus
    currentFocusedView?.let {
        inputMethodManager.hideSoftInputFromWindow(
            currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}

enum class ArrivesToEditItem{
    FROM_ITEM_LIST,
    FROM_ITEM_DETAILS
}

object Utils{
    lateinit var arrivesToEditItem:ArrivesToEditItem
}