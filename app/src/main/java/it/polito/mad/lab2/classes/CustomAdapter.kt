package it.polito.mad.lab2.classes

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.lab2.R
import it.polito.mad.lab2.activities.MainActivity
import it.polito.mad.lab2.classes.AddsArray.adds

//fragment is the fragment to which the adapter is associated.
//It can be ItemListFragment or OnSaleListFragment

enum class FragmentName{
    MY_ITEM_LIST,
    OTHERS_ON_SALE_LIST
}

class CustomAdapter(private val fragment:FragmentName) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_views, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(adds[position]!!, position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return adds.size
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }

    //the class is holding the list view
    //inner is needed for accessing attribute of CustomAdapter inside ViewHolder
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val viewImg = itemView.findViewById(R.id.cardImage) as ImageView
        val viewCard = itemView.findViewById(R.id.cardView) as CardView
        val textViewTitle = itemView.findViewById(R.id.cardTitle) as TextView
        val textViewCategory = itemView.findViewById(R.id.cardCategory) as TextView
        val textViewLocation = itemView.findViewById(R.id.cardLocation) as TextView
        val textViewExpiration = itemView.findViewById(R.id.cardDate) as TextView
        val textViewPrice = itemView.findViewById(R.id.cardPrice) as TextView
        val btnCardEdit = itemView.findViewById(R.id.cardEdit) as ImageButton
        val btnCardthrash = itemView.findViewById(R.id.cardThrash) as ImageButton

        fun bindItems(appData: Adds, indexToSend: Int) {

            viewImg.setImageURI(Uri.parse(appData.imgPath))
            textViewTitle.text = appData.title
            textViewCategory.text = appData.category
            textViewLocation.text = appData.location
            textViewExpiration.text = appData.expireDate
            textViewPrice.text = appData.price

            btnCardEdit.setOnClickListener {

                Utils.arrivesToEditItem = ArrivesToEditItem.FROM_ITEM_LIST

                val b = bundleOf(
                    AddsArray.ARG_ITEM_INDEX to indexToSend
                )
                it.findNavController().navigate(R.id.action_nav_adds_to_itemEditFragment, b)
            }

            //It doesn't work on itemView because it is under the other component
            viewCard.setOnClickListener {

                val b = bundleOf(
                    AddsArray.ARG_ITEM_INDEX to indexToSend
                )

                when(fragment){
                    FragmentName.MY_ITEM_LIST ->
                        it.findNavController().navigate(R.id.action_nav_adds_to_itemDetailsFragment, b)
                    FragmentName.OTHERS_ON_SALE_LIST ->
                        it.findNavController().navigate(R.id.action_onSaleListFragment_to_itemDetailsFragment, b)
                }

            }

            btnCardthrash.setOnClickListener {

                Snackbar.make(it, "You are about to delete this add", Snackbar.LENGTH_LONG)
                    .setAction("CONFIRM") {
                        adds.removeAt(adapterPosition)
                        val shared = it.context.getSharedPreferences(
                            MainActivity.SHARED_PREFS,
                            Context.MODE_PRIVATE
                        ) ?: return@setAction
                        AddsArray.persistAddsArray(shared)
                        it.findNavController().navigate(R.id.nav_adds)
                    }.show()
            }
        }

            fun unbind() {
                btnCardEdit.setOnClickListener(null)
                viewCard.setOnClickListener(null)
            }
    }
}
