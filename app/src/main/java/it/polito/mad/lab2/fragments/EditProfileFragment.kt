package it.polito.mad.lab2.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import it.polito.mad.lab2.R
import it.polito.mad.lab2.classes.Destination
import it.polito.mad.lab2.classes.ImageCaption
import it.polito.mad.lab2.classes.UserInfo
import it.polito.mad.lab2.classes.hideKeyboard
import kotlinx.android.synthetic.main.fragment_2_edit_profile.*


class EditProfileFragment : Fragment() {

    /****************************************
     *********** Global Variables ***********
     ****************************************/

    //Live data app attributes
    var profileImg = MutableLiveData<String>()
    var fullName = MutableLiveData<String>()
    var nickName = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var location = MutableLiveData<String>()
    var userRating = MutableLiveData<Double>()
    var itemBoughtNumber = MutableLiveData<Int>()

    //Variables for holding instance data
    private var SHARED_PREFS: String = "sharedPrefs"

    /*************************************
     ********* On Create View ************
     *************************************/

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //Enable options menu in fragment
        setHasOptionsMenu(true)

        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_2_edit_profile, container, false)

        //Binding the camera icon with the context menu
        registerForContextMenu(root.findViewById(R.id.srcPic))

        return root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(
            true // default to enabled
        ) {
            override fun handleOnBackPressed() {
                MaterialAlertDialogBuilder(context)
                    .setTitle("Warning")
                    .setMessage("You will loose all the unsaved changes.\nDo you want to continue?")
                    .setPositiveButton("OK"){
                            dialog: DialogInterface, which: Int ->
                        //Toast.makeText(context,"DONE",Toast.LENGTH_SHORT)
                        dialog.dismiss()
                        findNavController().navigateUp()
                        //super.onBackPressed()
                    }
                    .setNegativeButton("Cancel"){
                            dialog: DialogInterface, which: Int ->
                        //Toast.makeText(context,"CANCELED",Toast.LENGTH_SHORT)
                        dialog.cancel()
                    }
                    .create().show()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,  // LifecycleOwner
            callback
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fullName.value = arguments?.getString("editFullName")
        nickName.value = arguments?.getString("editNickName")
        email.value = arguments?.getString("editEmail")
        location.value = arguments?.getString("editLocation")
        profileImg.value = arguments?.getString("editProfileImg")

        srcPic.setOnClickListener {
            //Toast.makeText(this.context, "Change item image clicked", Toast.LENGTH_SHORT).show()
            requireActivity().openContextMenu(it)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //Restoring captured data
        if(savedInstanceState != null){
            profileImg.value = savedInstanceState.getString("profileImg")
            fullName.value = savedInstanceState.getString("fullName")
            nickName.value = savedInstanceState.getString("nickName")
            email.value = savedInstanceState.getString("email")
            location.value = savedInstanceState.getString("location")
        }

    }


    /*************************************
     ************* On Start **************
     *************************************/

    //Capturing data from user on real-time
    override fun onStart() {
        super.onStart()
        profileImg.observe(this, Observer { editImageView.setImageURI(Uri.parse(it)) })
        fullName.observe(this, Observer { editFullName.setText(it) })
        nickName.observe(this, Observer { editNickName.setText(it) })
        email.observe(this, Observer { editEmail.setText(it) })
        location.observe(this, Observer { editLocation.setText(it) })
        userRating.observe(this, Observer { tvRating.text = it.toString() })
        itemBoughtNumber.observe(this, Observer { tvNumberBought.text = it.toString() })
    }

    /*********************************************
     ********* On Save Instance State ************
     *********************************************/

    //Saving data before a pause ()
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //Saving the user data before a change on the activity
        outState.putString("profileImg", profileImg.value.toString())
        outState.putString("fullName", editFullName.text.toString())
        outState.putString("nickName", editNickName.text.toString())
        outState.putString("email", editEmail.text.toString())
        outState.putString("location", editLocation.text.toString())

    }

    // --> Disk icon option
    /****************************************************
     ********* Overriding Activity's Toolbar ************
     ****************************************************/

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_save, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    /****************************************************
     ************ Toolbar Options Selected   ************
     ****************************************************/

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            R.id.menu_save -> {
                //Saving the information of the user
                //in case there is missing or wrong data
                //stays on the editProfile otherwise
                //jumps to ShowProfile
                if (save()){
                    // explanation : if I am here, the user was on showProfile, then clicked the edit button
                    // and goes in editProfile  The first popBackStack remove editProfile from the backStack,
                    // the second one, remove also showProfile this is needed because the instruction navigate
                    // after the popBackStacks add another time showProfile to the backStack
                    findNavController().popBackStack()
                    findNavController().popBackStack()

                    findNavController().navigate(R.id.nav_profile)
                }
                true
            }
            else -> super.onOptionsItemSelected(menuItem)
        }
    }

    /****************************************************
     ************ Overriding Context Menu ***************
     ****************************************************/

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater: MenuInflater? = activity?.menuInflater
        //inflate method is the operation used to perform the inflation of menu object
        inflater?.inflate(R.menu.floating_menu, menu)
    }

    /*********************************************************
     ************ Context Menu Options Selected   ************
     *********************************************************/

    override fun onContextItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.gallery -> {
                ImageCaption.imageManager(requireContext(), requireActivity(), "gallery", Destination.EDIT_PROFILE_FRAGMENT, profileImg)
                true
            }
            R.id.camera -> {
                ImageCaption.imageManager(requireContext(), requireActivity(), "camera", Destination.EDIT_PROFILE_FRAGMENT, profileImg)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    // --> Disk icon actions after pressing
    /****************************************************
     *************     Saving the data    ***************
     ****************************************************/

    // Function that checks the information provided by the user
    // If all the information is correct, then save the data
    // through the instantiation of userInfo dataClass
    fun save(): Boolean {
        if (listOf(
                editFullName.text.length, editNickName.text.length, editEmail.text.length,
                editLocation.text.length
            ).all { it != 0 } && android.util.Patterns.EMAIL_ADDRESS.matcher(editEmail.text).matches()
        ) {

            hideKeyboard(requireActivity())

            //Capturing the data of the User
            val userData = UserInfo(
                profileImg.value.toString(),
                editFullName.text.toString(),
                editNickName.text.toString(),
                editEmail.text.toString(),
                editLocation.text.toString()
            )

            //Parsing the data-object as Json
            val gSon = Gson()
            var userInfo = gSon.toJson(userData)

            //Storing the captured variables back from EditProfileActivity
            val sharedPref = context?.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
            with(sharedPref?.edit()) {
                //Persisting the data
                this?.putBoolean("notEmpty", true)
                //Control key-value pair to know if there is prior persistance
                this?.putString("userPersist", userInfo)
                //Storing the Json object
                this?.commit()
                Log.e("Lab2Debug","Data stored correctly")
            }

            //update drawer headerLayout after editing the user profile
            val navView: NavigationView = requireActivity().findViewById(R.id.nav_view)
            val headerView = navView.getHeaderView(0)
            headerView.findViewById<ImageView>(R.id.imgView_nav_header).setImageURI(Uri.parse(userData.profileImg))
            headerView.findViewById<TextView>(R.id.tv_nav_header_title).text = userData.fullName
            headerView.findViewById<TextView>(R.id.tv_nav_header_subtitle).text = userData.email
            return true
        } else {

            if (editFullName.text.isEmpty()) {
                val toast =
                    Toast.makeText(context, "Complete full name field!", Toast.LENGTH_SHORT)
                toast.show()
                editFullName.requestFocus()
            } else if (editNickName.text.isEmpty()) {
                val toast =
                    Toast.makeText(context, "Complete nickname field!", Toast.LENGTH_SHORT)
                toast.show()
                editNickName.requestFocus()
            } else if (editEmail.text.isEmpty()) {
                val toast = Toast.makeText(context, "Complete email field!", Toast.LENGTH_SHORT)
                toast.show()
                editEmail.requestFocus()
            } else if (editLocation.text.isEmpty()) {
                val toast =
                    Toast.makeText(context, "Complete Location field!", Toast.LENGTH_SHORT)
                toast.show()
                editLocation.requestFocus()
            }
            else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(editEmail.text).matches()){
                val toast =
                    Toast.makeText(context, "Wrong email field!", Toast.LENGTH_SHORT)
                toast.show()
                editEmail.setError("Inserted email is wrong")
                editLocation.requestFocus()
            }
            return false
        }
    }

}






